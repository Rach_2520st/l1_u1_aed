#include <iostream>
#include "Contador.h"
#include <ctype.h>

using namespace std;

Contador::Contador(){
}

int Contador::get_lower(string frase){
	this->lower = 0; 
	for (int i = 0; i < frase.size(); i++)
	{
		//cuenta minusculas
		if (islower(frase[i]))
		{
			//va sumando a medida que encuentra minusculas
			this->lower++;
		}
	}
	//retorna las minusculas que encontró
	return this->lower;
}

//MAYUSCULAS
int Contador::get_upper(string frase){
	this->upper = 0;
	for (int i = 0; i < frase.size(); i++)
	{
		if (isupper(frase[i]))
		{
			this->upper++;
		}
	}
	//retorna las mayusculas
	return this->upper;
}