// g++ programa.cpp Contador.cpp -o programa
 
#include <iostream>
#include "Contador.h"
using namespace std;

void salida(string *frase, int limite){
	
	Contador c = Contador();

	
	for(int i = 0; i < limite; i++){
		
		cout << "La frase (" << i << "):" << endl;
		cout << "* " << frase[i] << endl;
		cout << "Tiene :" << c.get_lower(frase[i]) << " minusculas" << endl;
		cout << "Tiene :" << c.get_upper(frase[i]) << " mayusculas" << endl;
		
	}
}

int main()
{
	
	cout << "****************************BIENVENIDO**********************************" << endl;
	cout << "Este programa se encarga de contar el número de mayusculas y minúsculas" << endl;
	cout << "en un número de palabras que ingrese el usuario" << endl;
	
	int limite;
	cout << " ¿Cuántas palabras tiene la frase que quieres ingresar?";
	cin >> limite;


	string frase[limite];
	
	
	for (int i = 0; i < limite; i++){
		
		cout << "Ingresa la palabra(" << i +1 << "): ";
		cin >> frase[i];
	}

	
	salida(frase, limite);

	return 0;
}