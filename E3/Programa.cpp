//Programa.cpp
#include <iostream>
#include <stdio.h>
#include "Cliente.h"

using namespace std;

void imprimir_datos(int cant_clientes, Cliente *cliente){
	//recorre el arreglo que contiene la información de los clientes y la imprime
	for (int i = 0; i < cant_clientes; i++){
		
		cout << "**********DATOS CLIENTE**********" << endl;
		cout << "  *Nombre: " << cliente[i].get_nombre() << endl;
		cout << "  *Teléfono: " << cliente[i].get_telefono() << endl;
		cout << "  *Saldo de la deuda: " << cliente[i].get_saldo() << endl;
	
		//imprime el estado(si es moroso o no)
		if(cliente[i].get_estado()){
			cout << "    CLIENTE MOROSO, pague su deuda a la brevedad" << endl;
		}
		else{
			cout << "    CLIENTE SIN DEUDA, disfrute nuestros servicios:)" << endl;
		}
		
	}
}
	
void llenar_datos(int cant_clientes, Cliente *cliente){
	string nombre;
	int telefono;
	int saldo;
	string estado;

	for (int i = 0; i < cant_clientes; i++){
		
		cout << "CLIENTE n°" << i +1 << endl;
		cout << "Ingrese el nombre del cliente: " << endl;
		cin >> nombre;
		cout << "Ingrese el número de telefono del cliente: " << endl;
		cin >> telefono;
		cout << "ingrese el saldo del cliente: $ " << endl;
		cin >> saldo;
		cout << "¿El cliente es moroso? (s/n): " << endl;
		cin >> estado;

		while(estado != "s" && estado != "n"){
			cout << "Opcion invalida. SOLO DEBE INGRESAR s/n";
			cin >> estado;
		}
		
		cliente[i].set_nombre(nombre);
		cliente[i].set_telefono(telefono);
		cliente[i].set_saldo(saldo);

		if(estado == "s"){
			cliente[i].set_estado(1);
		}
		else{
			cliente[i].set_estado(0);
		}
	}
}

int main(){
	int cant_clientes;

	cout << "**************BIENVENIDO**************" << endl;
	cout << "Ingresa la cantidad de clientes que quieres anexar al sistema: ";
	cin >> cant_clientes;

	
	Cliente cliente[cant_clientes];
	llenar_datos(cant_clientes, cliente);
	imprimir_datos(cant_clientes, cliente);

	return 0;

}
