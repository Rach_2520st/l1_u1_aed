//Cliente.cpp
#include <iostream>
#include "Cliente.h"

using namespace std;

Cliente::Cliente(){
}

void Cliente::set_nombre(string nombre){
	this->nombre = nombre;
}

void Cliente::set_telefono(int telefono){
	this->telefono = telefono;
}

void Cliente::set_saldo(int saldo){
	this->saldo = saldo;
}

void Cliente::set_estado(bool estado){
	this->estado = estado;
}

string Cliente::get_nombre(){
	return this->nombre;
}

int Cliente::get_telefono(){
	return this->telefono;
}

int Cliente::get_saldo(){
	return this->saldo;
}

bool Cliente::get_estado(){
	return this->estado;
}
