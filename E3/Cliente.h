//Cliente.h
#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente{
	private:
		string nombre;
		int telefono;
		int saldo;
		bool estado;

	public:
		Cliente();

		string get_nombre();
		int get_telefono();
		
		void set_nombre(string nombre);
		void set_telefono(int telefono);
		void set_saldo(int saldo);
		void set_estado(bool estado);

		int get_saldo();
		bool get_estado();

};
#endif

