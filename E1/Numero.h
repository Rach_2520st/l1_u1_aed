//Numero.h
#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
	private:
		int max;
		int *vector = NULL;
		int resultado;

	public:
		
		Numero(int max);

		
		void operacion_SumaCuadrado(int *vector, int max);
		int get_resultado();

};
#endif
