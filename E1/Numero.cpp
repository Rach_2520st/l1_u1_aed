//Numero.cpp 
 
#include <iostream>
#include "Numero.h"

Numero::Numero(int max){


	this->max = max;
	this->vector = new int[this->max]; //Esta linea permite la creación de espacio en la memoria

}

void Numero::operacion_SumaCuadrado(int *vector, int max){
	/* variable a la que se le suman el cuadrado de los numeros */
	int resultado=0;
	for (int i = 0; i < max; ++i)
	{
		// este for eleva los numeros al cuadrado y los suma
		resultado += vector[i] * vector[i];
	}
	//entrega el resultado final de la operación
	this->resultado=resultado;
}

int Numero::get_resultado(){
	return this->resultado;
}