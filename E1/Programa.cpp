// g++ Programa.cpp Numero.cpp -o programa
 
#include <iostream>
#include "Numero.h"
using namespace std;

int main()
{

	cout << "**********BIENVENIDO**********" << endl;
	cout << "Este programa te permite obtener la suma de" << endl;
	cout << "los cuadrados de los números que decidas ingresar" << endl;

	int max;
	cout << "¿Cuántos números deseas ingresar?" << endl;
	cout << ": " << endl;
	cin >> max;
	
	int vector[max];
	
	for (int i = 0; i < max; ++i)
	{
		//este For permite llenar el arreglo con los numeros que se desean 
		//elevar y luego sumar
		cout << "Ingresa el número de la posición [" << i + 1 << "]: ";
		cin >> vector[i];
	}

	Numero n = Numero(max);
	n.operacion_SumaCuadrado(vector, max);
	
	cout << "El resultado obtenido es: " << n.get_resultado() << endl;
	
	return 0;
}
